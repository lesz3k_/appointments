package pl.domestic.appointments.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;

import pl.domestic.appointments.dao.UserCredentialsDAO;
import pl.domestic.appointments.dao.mapper.UserCredentialsMapper;
import pl.domestic.appointments.entity.UserCredentials;

public class UserCredentialsDAOImpl implements UserCredentialsDAO {

	@Autowired
	private UserCredentialsMapper userCredentialsMapper;

	@Override
	public String retrieveHash(String username) {
		return userCredentialsMapper.retrieveHash(username);
	}

	@Override
	public void storeUser(UserCredentials user) {
		userCredentialsMapper.storeUser(user);
	}

	public void setUserCredentialsMapper(UserCredentialsMapper userMapper) {
		this.userCredentialsMapper = userMapper;
	}

	@Override
	public void deleteCredentialsById(Integer id) {
		userCredentialsMapper.deleteCredentialsById(id);

	}
	
	@Override
	public Boolean getAdminFlag(String username) {
		return userCredentialsMapper.getAdminFlag(username);
	}

	@Override
	public boolean exists(String username) {
		return userCredentialsMapper.exists(username);
	}

}
