package pl.domestic.appointments.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.domestic.appointments.dao.EmployeeTypeDAO;
import pl.domestic.appointments.dao.mapper.EmployeeTypeMapper;
import pl.domestic.appointments.entity.EmployeeType;

public class EmployeeTypeDAOImpl implements EmployeeTypeDAO {
	
	@Autowired
	private EmployeeTypeMapper employeeTypeMapper;

	@Override
	public List<EmployeeType> getEmployeeTypes() {
		return employeeTypeMapper.getEmployeeTypes();
	}
	
	/**
	 * @param employeeTypeMapper the employeeTypeMapper to set
	 */
	public void setEmployeeTypeMapper(EmployeeTypeMapper employeeTypeMapper) {
		this.employeeTypeMapper = employeeTypeMapper;
	}

}
