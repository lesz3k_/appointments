package pl.domestic.appointments.dao.impl;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.domestic.appointments.dao.EventsDAO;
import pl.domestic.appointments.dao.mapper.EventsMapper;
import pl.domestic.appointments.entity.DiagnosisFeedback;
import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.Event;

public class EventsDAOImpl implements EventsDAO {

	@Autowired
	private EventsMapper eventsMapper;

	@Override
	public void createEvent(Event event) {
		eventsMapper.createEvent(event);
	}

	@Override
	public Integer updateEventStart(Event event) {
		return eventsMapper.updateEventStart(event);
	}

	@Override
	public Integer updateEventEnd(Event event) {
		return eventsMapper.updateEventEnd(event);
	}

	@Override
	public void updateEventNoshow(Event event) {
		eventsMapper.updateEventNoshow(event);
	}

	@Override
	public List<Event> getEvents(Date from, Date to, Employee employee) {
		return eventsMapper.getEvents(from, to, employee);
		
	}

	@Override
	public void createEventList(List<Event> eventList) {
		eventsMapper.createEventList(eventList);
	}

	@Override
	public DiagnosisFeedback getTopDiagnosisAndTimeslot(Date from, Date to) {
		return eventsMapper.getTopDiagnosisAndTimeslot(from, to);
	}

	@Override
	public void updateEvent(Event event) {
		eventsMapper.updateEvent(event);
	}

}
