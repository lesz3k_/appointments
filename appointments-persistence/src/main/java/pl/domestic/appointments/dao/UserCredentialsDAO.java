package pl.domestic.appointments.dao;

import pl.domestic.appointments.entity.UserCredentials;

public interface UserCredentialsDAO {
	
	/**
	 * Store newly created user credentials in database.
	 * @param username
	 * @param hash
	 */
	public void storeUser(UserCredentials user);

	/**
	 * Retrieve given user's hash.
	 * @param username whose hash should be retrieved.
	 * @return string representing the hashed password.
	 */
	public String retrieveHash(String username);
	
	/**
	 * Perform an existence check on a given user.
	 * @param username for the check to be made.
	 * @return true if exists; false if not.
	 */
	public boolean exists(String username);

	public void deleteCredentialsById(Integer id);
	
	public Boolean getAdminFlag(String username);

}
