package pl.domestic.appointments.dao;

import java.util.List;

import pl.domestic.appointments.entity.ICD10;

/**
 * Base interface defining operations on ICD10 codebase.
 * @author Leszek_Wisniewski
 *
 */
public interface DiagnosisDAO {
	
	/**
	 * Method retrieving all of ICD-10 codes along with their descriptions in polish language.
	 * @return a collection of ICD10 entities.
	 */
	List<ICD10> getICD10CodeList();

}
