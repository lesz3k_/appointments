package pl.domestic.appointments.dao;

import java.sql.Date;
import java.util.List;

import pl.domestic.appointments.entity.DiagnosisFeedback;
import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.Event;

public interface EventsDAO {
	
	public List<Event> getEvents(Date from, Date to, Employee employee);
	
	public void createEventList(List<Event> eventList);

	public void createEvent(Event event);

	public Integer updateEventStart(Event event);

	public Integer updateEventEnd(Event event);

	public void updateEventNoshow(Event event);
	
	public DiagnosisFeedback getTopDiagnosisAndTimeslot(Date from, Date to);

	public void updateEvent(Event event);


}
