package pl.domestic.appointments.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.domestic.appointments.dao.DiagnosisDAO;
import pl.domestic.appointments.dao.mapper.DiagnosisMapper;
import pl.domestic.appointments.entity.ICD10;

public class DiagnosisDAOImpl implements DiagnosisDAO {

	@Autowired
	private DiagnosisMapper diagnosisMapper;

	@Override
	public List<ICD10> getICD10CodeList() {

		return diagnosisMapper.getICD10CodeList();
		
	}

}
