package pl.domestic.appointments.dao;

import java.sql.Date;
import java.util.List;

import pl.domestic.appointments.entity.Employee;

public interface EmployeeDAO {
	
	public void storeEmployee(Integer employeeType, String firstname, String lastname, String pesel);
	
	public void storeEmployee(Employee employee);
	
	public List<Employee> getEmployees();

	public void deleteEmployeeById(Integer id);

	public Employee getEmployeeById(Integer id);

	public Employee getEmployeeByUsername(String username);

	public Employee getEmployeeSchedule(Employee employee, Date start,
			Date end);


}
