package pl.domestic.appointments.dao.impl;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.domestic.appointments.dao.EmployeeDAO;
import pl.domestic.appointments.dao.mapper.EmployeeMapper;
import pl.domestic.appointments.entity.Employee;

public class EmployeeDAOImpl implements EmployeeDAO {

	@Autowired
	private EmployeeMapper employeeMapper;

	@Override
	public void storeEmployee(Integer employeeType, String firstname, String lastname, String pesel) {
		//employeeMapper.storeEmployee(employeeType, firstname, lastname, pesel);
	}
	
	/**
	 * @param employeeMapper the employeeMapper to set
	 */
	public void setEmployeeMapper(EmployeeMapper employeeMapper) {
		this.employeeMapper = employeeMapper;
	}

	@Override
	public void storeEmployee(Employee employee) {
		employeeMapper.storeEmployee(employee);
	}

	@Override
	public List<Employee> getEmployees() {
		return employeeMapper.getEmployees();
	}

	@Override
	public void deleteEmployeeById(Integer id) {
		employeeMapper.deleteEmployeeById(id);
		
	}

	@Override
	public Employee getEmployeeById(Integer id) {
		return employeeMapper.getEmployeeById(id);
	}

	@Override
	public Employee getEmployeeByUsername(String username) {
		return employeeMapper.getEmployeeByUsername(username);
	}

	@Override
	public Employee getEmployeeSchedule(Employee employee, Date start,
			Date end) {
		return employeeMapper.getEmployeeSchedule(employee, start, end);
	}

}
