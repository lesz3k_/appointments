package pl.domestic.appointments.dao;

import java.util.List;

import pl.domestic.appointments.dao.mapper.PatientMapper;
import pl.domestic.appointments.entity.Patient;

public interface PatientDAO {
	
	public Patient getPatient(Integer id);
	
	public void setPatientMapper(PatientMapper patientMapper);

	public List<Patient> getAllPatients();
}
