package pl.domestic.appointments.dao;

import java.util.List;

import pl.domestic.appointments.entity.Action;

public interface ActionDAO {
	
	public List<Action> getActions(Integer id); 

}
