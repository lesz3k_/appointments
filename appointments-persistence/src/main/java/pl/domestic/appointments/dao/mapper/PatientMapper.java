package pl.domestic.appointments.dao.mapper;

import java.util.List;
import pl.domestic.appointments.entity.Patient;

public interface PatientMapper {

	public List<Patient> getAllPatients();
	
	public Patient getPatient(Integer id);

}