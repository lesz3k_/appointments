package pl.domestic.appointments.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import pl.domestic.appointments.entity.ReportType;

public interface ReportTypeMapper {

	List<ReportType> getAllReportTypes();
}
