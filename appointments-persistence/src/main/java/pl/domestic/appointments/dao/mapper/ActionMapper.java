package pl.domestic.appointments.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import pl.domestic.appointments.entity.Action;

public interface ActionMapper {

	public List<Action> getActionsById(Integer employee_typeid);
}
