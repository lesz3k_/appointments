package pl.domestic.appointments.dao;

import java.util.List;

import pl.domestic.appointments.entity.EmployeeType;

public interface EmployeeTypeDAO {
	
	public List<EmployeeType> getEmployeeTypes();

}
