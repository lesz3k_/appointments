package pl.domestic.appointments.dao.mapper;

import java.sql.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import pl.domestic.appointments.entity.Employee;

public interface EmployeeMapper {

	public void storeEmployee(Employee employee);
	
	public List<Employee> getEmployees();
	
	public void deleteEmployeeById(Integer id);

	public Employee getEmployeeById(Integer id);

	public Employee getEmployeeByUsername(String username);

	public Employee getEmployeeSchedule(@Param("employee")Employee employee, @Param("start")Date start,
			@Param("end")Date end);

}
