package pl.domestic.appointments.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.domestic.appointments.dao.ActionDAO;
import pl.domestic.appointments.dao.mapper.ActionMapper;
import pl.domestic.appointments.entity.Action;

public class ActionDAOImpl implements ActionDAO {

	@Autowired
	private ActionMapper actionMapper;

	@Override
	public List<Action> getActions(Integer id) {
		return actionMapper.getActionsById(id);
	}
	
	/**
	 * @return the actionsMapper
	 */
	public ActionMapper getActionMapper() {
		return actionMapper;
	}

	/**
	 * @param actionsMapper the actionsMapper to set
	 */
	public void setActionMapper(ActionMapper actionMapper) {
		this.actionMapper = actionMapper;
	}

}
