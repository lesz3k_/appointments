package pl.domestic.appointments.dao.mapper;

import java.util.List;

import pl.domestic.appointments.entity.ICD10;

public interface DiagnosisMapper {
	
	public List<ICD10> getICD10CodeList();

}
