package pl.domestic.appointments.dao.mapper;

import java.sql.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import pl.domestic.appointments.entity.DiagnosisFeedback;
import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.Event;

public interface EventsMapper {
	
	public List<Event> getEvents(@Param("from")Date from, @Param("to")Date to, @Param("employee")Employee employee);
	
	public void createEvent(Event event);
	
	public Integer updateEventStart(Event event);
	
	public Integer updateEventEnd(Event event);
	
	public Event updateEventNoshow(Event event);

	public void createEventList(@Param("eventList")List<Event> eventList);

	public DiagnosisFeedback getTopDiagnosisAndTimeslot(@Param("start") Date from, @Param("end") Date to);

	public List<Event> getEventsForDate(@Param("date")Date date, @Param("employee")Employee employee);

	public void updateEvent(Event event);
	
}
