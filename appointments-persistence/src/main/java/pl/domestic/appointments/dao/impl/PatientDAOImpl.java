package pl.domestic.appointments.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.domestic.appointments.entity.Patient;
import pl.domestic.appointments.dao.PatientDAO;
import pl.domestic.appointments.dao.mapper.PatientMapper;

public class PatientDAOImpl implements PatientDAO {

	@Autowired
	private PatientMapper patientMapper;

	/**
	 * @return the patientMapper
	 */
	public PatientMapper getPatientMapper() {
		return patientMapper;
	}

	/**
	 * @param patientMapper the patientMapper to set
	 */
	public void setPatientMapper(PatientMapper patientMapper) {
		this.patientMapper = patientMapper;
	}

	public Patient getPatient(Integer id) {
		return patientMapper.getPatient(id);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	@Override
	public List<Patient> getAllPatients() {
		return patientMapper.getAllPatients();
	}

}
