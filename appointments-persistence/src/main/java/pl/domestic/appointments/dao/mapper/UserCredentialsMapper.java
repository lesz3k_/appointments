package pl.domestic.appointments.dao.mapper;

import pl.domestic.appointments.entity.UserCredentials;

public interface UserCredentialsMapper {

	String retrieveHash(String username);
	
	Boolean getAdminFlag(String username);
	
	void storeUser(UserCredentials userCredentials);

	void deleteCredentialsById(Integer id);

	boolean exists(String username);

}
