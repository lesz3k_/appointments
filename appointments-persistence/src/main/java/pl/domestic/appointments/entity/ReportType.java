package pl.domestic.appointments.entity;

public class ReportType {

	private Integer id;
	private String reportType;

	/**
	 * @return the reportTypeId
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param reportTypeId
	 *            the reportTypeId to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @param reportType
	 *            the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("[").append("ID =").append(id)
				.append(", report type = ").append(reportType).append("]");
		return sb.toString();
	}
}
