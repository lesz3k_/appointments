package pl.domestic.appointments.entity;

public class Action {

	private Integer id;
	private EmployeeType employeeType;
	private String action;
	
	public Action() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the actionsId
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param actionsId
	 *            the actionsId to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the employeeTypeId
	 */
	public EmployeeType getEmployeeType() {
		return employeeType;
	}

	/**
	 * @param employeeTypeId
	 *            the employeeTypeId to set
	 */
	public void setEmployeeType(EmployeeType employeeType) {
		this.employeeType = employeeType;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action
	 *            the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

}
