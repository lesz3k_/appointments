package pl.domestic.appointments.entity;

import java.util.List;

public class EmployeeType {
	
	private Integer id;
	private String type;
	private List<Employee> employees;
	
	public EmployeeType() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the employees
	 */
	public List<Employee> getEmployees() {
		return employees;
	}

	/**
	 * @param employees the employees to set
	 */
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	/**
	 * @return the employee_typeId
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * @param employee_typeId the employee_typeId to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	

}
