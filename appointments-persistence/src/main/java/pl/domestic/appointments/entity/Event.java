package pl.domestic.appointments.entity;

import java.util.Date;

public class Event {
	
	private Integer id;
	private Employee employee;
	private Patient patient;
	private Action action;
	private Date startPlanned;
	private Date endPlanned;
	private Date startActual;
	private Date endActual;
	private Boolean noshow;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Patient getPatient() {
		return patient;
	}
	
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	public Action getAction() {
		return action;
	}
	
	public void setAction(Action action) {
		this.action = action;
	}
	
	public Date getStartPlanned() {
		return startPlanned;
	}
	
	public void setStartPlanned(Date startPlanned) {
		this.startPlanned = startPlanned;
	}
	
	public Date getEndPlanned() {
		return endPlanned;
	}
	
	public void setEndPlanned(Date endPlanned) {
		this.endPlanned = endPlanned;
	}
	
	public Date getStartActual() {
		return startActual;
	}
	
	public void setStartActual(Date startActual) {
		this.startActual = startActual;
	}
	
	public Date getEndActual() {
		return endActual;
	}
	
	public void setEndActual(Date endActual) {
		this.endActual = endActual;
	}

	/**
	 * @return the noshow
	 */
	public Boolean getNoshow() {
		return noshow;
	}

	/**
	 * @param noshow the noshow to set
	 */
	public void setNoshow(Boolean noshow) {
		this.noshow = noshow;
	}

	/**
	 * @return the schedule
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * @param schedule the schedule to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	
}
