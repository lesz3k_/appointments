package pl.domestic.appointments.entity;

import java.sql.Date;

public class DiagnosisFeedback {
	
	private ICD10 icd10;
	private Integer timeslot;
	
	/**
	 * @return the idc10
	 */
	public ICD10 getIcd10() {
		return icd10;
	}
	/**
	 * @param idc10 the idc10 to set
	 */
	public void setIcd10(ICD10 icd10) {
		this.icd10 = icd10;
	}
	/**
	 * @return the timeslot
	 */
	public Integer getTimeslot() {
		return timeslot;
	}
	/**
	 * @param timeslot the timeslot to set
	 */
	public void setTimeslot(Integer timeslot) {
		this.timeslot = timeslot;
	}
}
