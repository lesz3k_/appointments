package pl.domestic.appointments.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.Event;

@ContextConfiguration("classpath:test-persistence.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class ScheduleDAOTest {
//
//	@Autowired
//	private ScheduleDAO dao;
//
//	private Employee employee;

//	@Before
//	public void init() {
//		employee = new Employee();
//		employee.setId(1);
//	}
//
//	@Test
//	public void testCreateNewScheduleEntry() throws Exception {
//
//		Schedule schedule = new Schedule();
//		schedule.setDate(new Date(new DateTime().getMillis()));
//		schedule.setEmployee(employee);
//
//		dao.createEmployeeSchedule(schedule);
//
//		assertNotNull(schedule.getId());
//
//	}
//
//	@Test
//	public void testGetEmployeeScheduleBySingleDate() throws Exception {
//
//		DateTime stdDate = new DateTime(2015, 04, 03, 00, 00);
//		Date date = new Date(stdDate.getMillis());
//
//		Schedule schedule = dao.getEmployeeScheduleForDate(employee, date);
//
//		List<Event> eventList = schedule.getEventList();
//
//		assertNotNull(eventList);
//		assertEquals(eventList.size(), 3);
//
//	}
//
//	@Test
//	public void testUpdateEmployeeSchedule() throws Exception {
//		DateTime stdDate = new DateTime(2015, 04, 03, 00, 00);
//		Date date = new Date(stdDate.getMillis());
//
//		Schedule schedule = dao.getEmployeeScheduleForDate(employee, date);
//
//		DateTime newStdDate = new DateTime(2015, 04, 03, 00, 00);
//		Date newDate = new Date(newStdDate.getMillis());
//
//		schedule.setDate(newDate);
//
//		dao.updateEmployeeSchedule(schedule);
//
//		assertTrue(schedule.getDate().equals(newDate));
//	}
//
//	@Test
//	public void testGetEmployeeScheduleBetweenDates() throws Exception {
//
//		DateTime startDate = new DateTime(2015, 04, 03, 00, 00);
//		DateTime endDate = new DateTime(2015, 04, 04, 00, 00);
//
//		Date start = new Date(startDate.getMillis());
//		Date end = new Date(endDate.getMillis());
//
//		List<Schedule> scheduleList = dao.getEmployeeSchedule(employee, start,
//				end);
//
//		assertNotNull(scheduleList);
//		assertEquals(scheduleList.size(), 2);
//	}
//
//	@Test
//	public void testGetEmployeeScheduleWithoutDates() throws Exception {
//
//		List<Schedule> allEmployeeSchedules = dao.getEmployeeSchedule(employee,
//				null, null);
//
//		assertNotNull(allEmployeeSchedules);
//		assertEquals(allEmployeeSchedules.size(), 5);
//
//	}
//
//	@Test
//	public void testGetEmployeeScheduleWithBeginDate() throws Exception {
//
//		DateTime startDate = new DateTime(2015, 04, 03, 00, 00);
//		Date start = new Date(startDate.getMillis());
//		
//		List<Schedule> allEmployeeSchedules = dao.getEmployeeSchedule(employee,
//				start, null);
//
//		assertNotNull(allEmployeeSchedules);
//		assertEquals(allEmployeeSchedules.size(), 2);
//	}
//
//	@Test
//	public void testGetEmployeeScheduleWithEndDate() throws Exception {
//		
//		DateTime endDate = new DateTime(2015, 04, 02, 00, 00);
//		Date end = new Date(endDate.getMillis());
//		
//		List<Schedule> allEmployeeSchedules = dao.getEmployeeSchedule(employee,
//				null, end);
//
//		assertNotNull(allEmployeeSchedules);
//		assertEquals(allEmployeeSchedules.size(), 3);
//
//	}
//
//	@Test
//	public void testDeleteEmployeeSchedule() throws Exception {
//
//		DateTime stdDate = new DateTime(2015, 04, 03, 00, 00);
//		Date date = new Date(stdDate.getMillis());
//
//		Schedule schedule = dao.getEmployeeScheduleForDate(employee, date);
//
//		dao.deleteEmployeeSchedule(schedule);
//
//		Schedule shouldBeNull = dao.getEmployeeScheduleForDate(employee, date);
//
//		assertNull(shouldBeNull);
//	}
//
//	@After
//	public void breakdown() {
//		employee = null;
//	}

}
