package pl.domestic.appointments.dao;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;



import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.domestic.appointments.entity.EmployeeType;

@ContextConfiguration("classpath:test-persistence.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class EmployeeTypeDAOTest {
	
	@Autowired
	private EmployeeTypeDAO employeeTypeDAO;
	
	@Test
	public void testGetEmployeeTypes() throws Exception {
		
		List<EmployeeType> employeeTypes = employeeTypeDAO.getEmployeeTypes();
		
		assertTrue(employeeTypes != null);
		assertEquals(employeeTypes.size(), 2);
	}

}
