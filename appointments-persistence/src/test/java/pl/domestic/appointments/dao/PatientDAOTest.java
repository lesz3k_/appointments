package pl.domestic.appointments.dao;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.domestic.appointments.entity.Patient;

@ContextConfiguration("classpath:test-persistence.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class PatientDAOTest {
	
	@Autowired
	private PatientDAO patientDAO;
	
	@Test
	public void testGetAllPatients() throws Exception {
		List<Patient> patientList = patientDAO.getAllPatients();
		
		assertNotNull(patientList);
	}
	
	@Test
	public void testGetPatientById() throws Exception {
		Patient patient = patientDAO.getPatient(2);
		
		assertNotNull(patient);
	}
	
	

}
