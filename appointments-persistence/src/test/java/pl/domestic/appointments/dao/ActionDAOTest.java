package pl.domestic.appointments.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.domestic.appointments.entity.Action;

@ContextConfiguration("classpath:test-persistence.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class ActionDAOTest {
	
	@Autowired
	private ActionDAO actionDAO;
	
	@Test
	public void testGetAllActions() throws Exception {
		List<Action> actionList = actionDAO.getActions(1);
			
		assertTrue(actionList != null);
		
		assertEquals(actionList.size(), 3);
	}

}
