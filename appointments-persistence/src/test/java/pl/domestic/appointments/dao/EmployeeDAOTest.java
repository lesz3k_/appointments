package pl.domestic.appointments.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.EmployeeType;
import pl.domestic.appointments.entity.Event;

@ContextConfiguration("classpath:test-persistence.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class EmployeeDAOTest {
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Test
	public void testGetEmployees() throws Exception {
		List<Employee> employeeList = employeeDAO.getEmployees();
		
		assertTrue(employeeList != null);
		
		assertEquals(employeeList.size(), 4);
	}
	
	@Test
	public void testGetEmployeeById() throws Exception {
		Employee employee = employeeDAO.getEmployeeById(24);
		
		assertNotNull(employee);
		
	}
	
	@Test
	public void testStoreEmployee() throws Exception {
		
		EmployeeType employeeType = new EmployeeType();
		employeeType.setId(1);
		
		Employee employee = new Employee();
		employee.setFirstname("Test");
		employee.setLastname("Testowy");
		employee.setPesel("12345678901");
		employee.setEmployeeType(employeeType);
		
		employeeDAO.storeEmployee(employee);
		
		Integer id = employee.getId();
		assertNotNull(id);
		
		Employee storedEmployee = employeeDAO.getEmployeeById(id);
		assertNotNull(storedEmployee);
	
	}
	
	@Test
	public void testGetEmployeerScheduleFromNullDates() throws Exception {
		Employee employee = new Employee();
		employee.setId(1);
		
		Employee employeeWSchedule = employeeDAO.getEmployeeSchedule(employee, null, null);
		
		List<Event> everyScheduleRecordList = employeeWSchedule.getEventList();
		
		assertNotNull(everyScheduleRecordList);
		assertEquals(everyScheduleRecordList.size(), 1);
	}
	
	@Test
	public void testGetEmployeeByUsername() throws Exception {
		Employee employee = employeeDAO.getEmployeeByUsername("admin");
		assertTrue(employee != null);
	}

}
