package pl.domestic.appointments.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.domestic.appointments.entity.ICD10;

@ContextConfiguration("classpath:test-persistence.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class DiagnosisDAOTest {
	
	@Autowired
	private DiagnosisDAO dao;
	
	@Test
	public void testGetICD10CodeList() throws Exception {
		List<ICD10> list = dao.getICD10CodeList();
		
		assertNotNull(list);
		assertTrue(list.size() > 12000);
	}

}
