package pl.domestic.appointments.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.AssertThrows;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.EmployeeType;
import pl.domestic.appointments.entity.UserCredentials;

@ContextConfiguration("classpath:test-persistence.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class UserCredentialsDAOTest {
	
	@Autowired
	private UserCredentialsDAO userCredentialsDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Test
	public void testRetrieveHash() throws Exception {
		String hash = userCredentialsDAO.retrieveHash("admin");
		assertEquals(hash, "1000:3091d81c2c1d0ec84f5e59386e4d95e8b69a27a1cf03e8b0:54c7bd477d1374ca66f71b7f5bf5cec8a51305bbc4968e18");
	}
	
	@Test
	public void testStoreUser() throws Exception {
		
		EmployeeType employeeType = new EmployeeType();
		employeeType.setId(1);
		
		Employee employee = new Employee();
		employee.setFirstname("Test");
		employee.setLastname("Testowy");
		employee.setPesel("12345678901");
		employee.setEmployeeType(employeeType);
		
		employeeDAO.storeEmployee(employee);
		
		Integer id = employee.getId();
		assertNotNull(id);
		
		Employee storedEmployee = employeeDAO.getEmployeeById(id);
		assertNotNull(storedEmployee);
		
		UserCredentials credentials = new UserCredentials();
		credentials.setHash("lala");
		credentials.setUsername("ala");
		credentials.setAdmin(false);
		
		userCredentialsDAO.storeUser(credentials);
		
		
	}
	
	@Test
	public void testDeleteCredentialsById() throws Exception {
		EmployeeType employeeType = new EmployeeType();
		employeeType.setId(1);
		
		Employee employee = new Employee();
		employee.setFirstname("Test");
		employee.setLastname("Testowy");
		employee.setPesel("12345678901");
		employee.setEmployeeType(employeeType);
		
		employeeDAO.storeEmployee(employee);
		
		Integer id = employee.getId();
		assertNotNull(id);
		
		Employee storedEmployee = employeeDAO.getEmployeeById(id);
		assertNotNull(storedEmployee);
		
		UserCredentials credentials = new UserCredentials();
		credentials.setId(id);
		credentials.setHash("lala");
		credentials.setUsername("ala");
		
		userCredentialsDAO.storeUser(credentials);
		
		assertNotNull(userCredentialsDAO.retrieveHash("ala"));

		userCredentialsDAO.deleteCredentialsById(id);
		
		assertNull(userCredentialsDAO.retrieveHash("ala"));
	}
	
	@Test
	public void testExists() throws Exception {
		
		String admin = "admin";
		boolean exists = userCredentialsDAO.exists(admin);
		assertTrue(exists);
		
	}

}
