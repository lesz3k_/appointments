package pl.domestic.appointments.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.domestic.appointments.entity.Action;
import pl.domestic.appointments.entity.DiagnosisFeedback;
import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.Event;
import pl.domestic.appointments.entity.Patient;

@ContextConfiguration("classpath:test-persistence.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class EventsDAOTest {

	@Autowired
	private EventsDAO dao;

	private Employee employee;
	private Action action;
	private Patient patient;

	@Before
	public void init() {

		action = new Action();
		action.setId(3);

		patient = new Patient();
		patient.setId(7);

		employee = new Employee();
		employee.setId(1);
	}

	// @Test
	// public void testGetEventsInSchedule() throws Exception {
	// List<Event> eventList = dao.getEvents(to, from, employee);
	//
	// assertNotNull(eventList);
	// assertEquals(eventList.size(), 3);
	// }

	@Test
	public void testInsertListofEvents() throws Exception {
		List<Event> eventList = new ArrayList<Event>(2);

		DateTime firstStart = new DateTime(2015, 04, 02, 12, 15);
		DateTime firstEnd = new DateTime(2015, 04, 02, 12, 25);

		Event first = new Event();
		first.setAction(action);
		first.setPatient(patient);
		first.setEmployee(employee);
		first.setStartPlanned(new Date(firstStart.getMillis()));
		first.setEndPlanned(new Date(firstEnd.getMillis()));
		eventList.add(first);

		DateTime secondStart = new DateTime(2015, 04, 02, 12, 30);
		DateTime secondEnd = new DateTime(2015, 04, 02, 12, 40);

		Event second = new Event();
		Action anotherAction = new Action();
		anotherAction.setId(6);

		Patient anotherPatient = new Patient();
		anotherPatient.setId(3);

		second.setAction(anotherAction);
		second.setPatient(anotherPatient);
		second.setEmployee(employee);
		second.setStartPlanned(new Date(secondStart.getMillis()));
		second.setEndPlanned(new Date(secondEnd.getMillis()));

		eventList.add(second);

		dao.createEventList(eventList);

	}

	@Test
	public void testGetTopDiagnosisAndTimeslot() throws Exception {

		// 2015-04-02
		Date from = new Date(1427932800000l);
		// 2015-05-31
		Date to = new Date(1433030400000l);

		DiagnosisFeedback feedback = dao.getTopDiagnosisAndTimeslot(from, to);

		assertNotNull(feedback);
		assertTrue(feedback.getIcd10().getCode().equals("A00.0"));
		assertTrue(feedback.getTimeslot().equals(523));
	}

	@Test
	public void testCreateNewScheduleEntry() throws Exception {

		Event event = new Event();

		event.setStartPlanned(new Date(new DateTime().getMillis()));
		event.setPatient(patient);
		event.setEmployee(employee);
		event.setAction(action);

		dao.createEvent(event);

		assertNotNull(event.getId());

	}

//	 @Test
//	 public void testUpdateEmployeeSchedule() throws Exception {
//	 DateTime startDate = new DateTime(2015, 06, 15, 00, 00);
//	 DateTime endDate = new DateTime(2015, 06, 15, 23, 59);
//
//	 Date from = new Date(startDate.getMillis());
//	 Date to = new Date(endDate.getMillis());
//	
//	 List<Event> eventList = dao.getEvents(from, to, employee);
//	
//	 DateTime newStdDate = new DateTime(2015, 04, 03, 00, 00);
//	 Date newDate = new Date(newStdDate.getMillis());
//	
//	 eventList.setDate(newDate);
//	
//	 dao.updateEmployeeSchedule(eventList);
//	
//	 assertTrue(eventList.getDate().equals(newDate));
//	 }
	//
	 @Test
	 public void testGetEmployeeScheduleBetweenDates() throws Exception {
	
	 DateTime startDate = new DateTime(2015, 06, 15, 00, 00);
	 DateTime endDate = new DateTime(2015, 06, 17, 00, 00);
	
	 Date start = new Date(startDate.getMillis());
	 Date end = new Date(endDate.getMillis());
	
	 List<Event> eventList = dao.getEvents(start, end, employee);
	
	 assertNotNull(eventList);
	 assertEquals(eventList.size(), 1);
	 }
	//
	// @Test
	// public void testGetEmployeeScheduleWithoutDates() throws Exception {
	//
	// List<Schedule> allEmployeeSchedules = dao.getEmployeeSchedule(employee,
	// null, null);
	//
	// assertNotNull(allEmployeeSchedules);
	// assertEquals(allEmployeeSchedules.size(), 5);
	//
	// }
	//
	// @Test
	// public void testGetEmployeeScheduleWithBeginDate() throws Exception {
	//
	// DateTime startDate = new DateTime(2015, 04, 03, 00, 00);
	// Date start = new Date(startDate.getMillis());
	//
	// List<Schedule> allEmployeeSchedules = dao.getEmployeeSchedule(employee,
	// start, null);
	//
	// assertNotNull(allEmployeeSchedules);
	// assertEquals(allEmployeeSchedules.size(), 2);
	// }
	//
	// @Test
	// public void testGetEmployeeScheduleWithEndDate() throws Exception {
	//
	// DateTime endDate = new DateTime(2015, 04, 02, 00, 00);
	// Date end = new Date(endDate.getMillis());
	//
	// List<Schedule> allEmployeeSchedules = dao.getEmployeeSchedule(employee,
	// null, end);
	//
	// assertNotNull(allEmployeeSchedules);
	// assertEquals(allEmployeeSchedules.size(), 3);
	//
	// }
	//
	// @Test
	// public void testDeleteEmployeeSchedule() throws Exception {
	//
	// DateTime stdDate = new DateTime(2015, 04, 03, 00, 00);
	// Date date = new Date(stdDate.getMillis());
	//
	// Schedule schedule = dao.getEmployeeScheduleForDate(employee, date);
	//
	// dao.deleteEmployeeSchedule(schedule);
	//
	// Schedule shouldBeNull = dao.getEmployeeScheduleForDate(employee, date);
	//
	// assertNull(shouldBeNull);
	// }
	//
	// @After
	// public void breakdown() {
	// employee = null;
	// }

	@After
	public void breakdown() {
		employee = null;
		action = null;
		patient = null;
	}
}
