package pl.domestic.appointments.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import pl.domestic.appointments.entity.Action;
import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.EmployeeType;
import pl.domestic.appointments.service.ActionService;
import pl.domestic.appointments.service.EmployeeService;

import com.google.gson.Gson;
/**
 * Servlet implementation class EmployeeServlet
 */
@WebServlet("/EmployeeServlet")
public class EmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final static String CONTENT_TYPE = "application/json; charset=utf-8";
	
	private EmployeeService employeeService;
	
	private ActionService actionService;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmployeeServlet() {
		WebApplicationContext ctx = ContextLoader
				.getCurrentWebApplicationContext();
		this.employeeService = (EmployeeService) ctx
				.getBean("employeeService");
		
		this.actionService = (ActionService) ctx.getBean("actionService");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if (request.getParameter("delete") != null) {
			Integer id = Integer.valueOf(request.getParameter("delete"));
			employeeService.deleteEmployee(id);
			response.sendRedirect("users.jsp");
		} else if (request.getParameter("username") != null
				&& request.getParameter("checkUser") != null) {
			
		} else if (request.getParameter("selectedEmployeeId") != null) {
			Integer id = Integer.valueOf(request.getParameter("selectedEmployeeId"));
			
//			List<Action> actionList = serviceFacade.getActions(id);
			List<Action> actionList = actionService.getActions(id);
			response.setContentType(CONTENT_TYPE);
			Gson gson = new Gson();
			String json = gson.toJson(actionList);
			PrintWriter out = response.getWriter();
			
			IOUtils.write(json, out);
			IOUtils.closeQuietly(out);

		} else if (request.getParameter("getEmployees") != null) {

			List<Employee> employeeList = null;
			employeeList = employeeService.getAllEmployees();
			request.setAttribute("employees", employeeList);
		} else if (request.getParameter("getEmployeeTypes") != null) {
			List<EmployeeType> employeeTypeList = employeeService
					.getEmployeeTypes();
			request.setAttribute("employeeTypes", employeeTypeList);
		}

	}

}
