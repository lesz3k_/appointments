package pl.domestic.appointments.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.EmployeeType;
import pl.domestic.appointments.entity.UserCredentials;
import pl.domestic.appointments.service.AuthenticationService;
import pl.domestic.appointments.service.auth.exception.AuthenticationException;
import pl.domestic.appointments.service.auth.exception.UserAlreadyExistsException;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/AuthenticationServlet")
public class AuthenticationServlet extends HttpServlet {
	
	private static Logger logger = LoggerFactory
			.getLogger(AuthenticationServlet.class);
	
	private static final long serialVersionUID = 1L;

	private static final String ERROR_PARAM = "errorMessage";
	private static final String ERROR_MESSAGE = "Niepoprawne dane logowania. Spróbuj ponownie.";
	private static final String LOGIN_PARAM = "username";
	private static final String PASSWORD_PARAM = "password";
	private static final String CREATE_USER = "create_user";
	private static final String HOME_URL = "schedule.jsp";
	private static final String LOGIN_URL = "login.jsp";
	private static final String USERS_URL = "users.jsp";
	// expire session after 10 mins
	private static final int SESSION_EXP = 10 * 60;

	//JW: use final for assigned in constructor only (JVM Memory model bonus) 
	private AuthenticationService authenticationService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AuthenticationServlet() {
		super();
		WebApplicationContext ctx = ContextLoader
				.getCurrentWebApplicationContext();
		this.authenticationService = (AuthenticationService) ctx
				.getBean("authenticationService");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		processRequest(request, response);

	}

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.setCharacterEncoding("UTF-8");
		
		String username = request.getParameter(LOGIN_PARAM);
		String password = request.getParameter(PASSWORD_PARAM);
		//JW: Może lepiej osobny servlet do tworzenia samego użytkownika? 
		if (request.getParameter(CREATE_USER) != null) {
			// create new user
			try {
				Boolean isAdmin = false;
				
				String firstname = request.getParameter("firstname");
				String lastname = request.getParameter("lastname");
				String pesel = request.getParameter("pesel");
				Integer employeeTypeId = Integer.parseInt(request.getParameter("employeeType"));
				
				String adminCheckBox = request.getParameter("adminCheckBox");
				if(adminCheckBox != null) {
					isAdmin = Boolean.parseBoolean(request.getParameter("adminCheckBox"));
				}
				
				EmployeeType employeeType = new EmployeeType();
				employeeType.setId(employeeTypeId);
				
				Employee employee = new Employee();
				employee.setEmployeeType(employeeType);
				employee.setFirstname(firstname);
				employee.setLastname(lastname);
				employee.setPesel(pesel);
				
				UserCredentials userCredentials = authenticationService.generateCredentials(username, password);
				userCredentials.setAdmin(isAdmin);
				
				employee.setUserCredentials(userCredentials);
				//JW: Zawsze się udaje to stworzenie użytkownika? Trochę fire&forget
				authenticationService.createNewUser(employee);
				
				response.sendRedirect(USERS_URL);
			} catch (AuthenticationException e) {
				logger.error("An exception while creating user has been made.", e);
				throw new ServletException(e);
			} catch (UserAlreadyExistsException e) {
				logger.error("User is already in the database.", e);
				request.setAttribute(ERROR_PARAM, "Użytkownik już istnieje!");
				RequestDispatcher rd = request.getRequestDispatcher(USERS_URL);
                rd.forward(request, response); 
			}
		} else {
			// just validate
			boolean isValid = false;
			try {
				isValid = authenticationService.validateUser(username, password, true);

			} catch (AuthenticationException e) {
				logger.error("An exception while validating user has been made.", e);
				throw new ServletException(e);
			}

			HttpSession session = request.getSession();
			if (isValid) {
				session.setAttribute(LOGIN_PARAM, username);
				session.setMaxInactiveInterval(SESSION_EXP);
				Cookie userName = new Cookie(LOGIN_PARAM, username);
				userName.setMaxAge(SESSION_EXP);
				response.addCookie(userName);
				response.sendRedirect(HOME_URL);
			} else {
				session.invalidate();
				request.setAttribute(ERROR_PARAM, ERROR_MESSAGE);
				RequestDispatcher rd = request.getRequestDispatcher(LOGIN_URL);
                rd.forward(request, response); 
				
			}
		}

	}

}
