package pl.domestic.appointments.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import pl.domestic.appointments.entity.DiagnosisFeedback;
import pl.domestic.appointments.service.EventService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class EventServlet
 */
@WebServlet("/TimeslotServlet")
public class TimeslotServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private EventService eventService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TimeslotServlet() {
		WebApplicationContext ctx = ContextLoader
				.getCurrentWebApplicationContext();
		this.eventService = (EventService) ctx.getBean("eventService");

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		String recommendTimeslot = request.getParameter("recommendTimeslot");

		if (recommendTimeslot.equals("true")) {
			Date from = new Date(Long.parseLong(request.getParameter("from")
					+ "000"));
			Date to = new Date(Long.parseLong(request.getParameter("to")
					+ "000"));

			DiagnosisFeedback feedback = eventService
					.getTopDiagnosisAndTimeslot(from, to);

			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(feedback);

			PrintWriter pw = response.getWriter();
			pw.print(json);
			pw.close();
		}

	}

}
