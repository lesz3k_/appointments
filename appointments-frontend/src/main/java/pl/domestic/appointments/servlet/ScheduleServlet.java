package pl.domestic.appointments.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import pl.domestic.appointments.entity.Action;
import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.Event;
import pl.domestic.appointments.entity.Patient;
import pl.domestic.appointments.service.EventService;
import pl.domestic.appointments.service.PatientService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class ScheduleServlet
 */
@WebServlet("/ScheduleServlet")
public class ScheduleServlet extends HttpServlet {

	private static Logger logger = LoggerFactory
			.getLogger(ScheduleServlet.class);

	private static final long serialVersionUID = 1L;

	private final static String CONTENT_TYPE = "application/json; charset=utf-8";
	private final static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";
	private final static String CEST_OFFSET = "+0200";

	private PatientService patientService;

	private EventService eventService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ScheduleServlet() {
		WebApplicationContext ctx = ContextLoader
				.getCurrentWebApplicationContext();

		this.patientService = (PatientService) ctx.getBean("patientService");

		this.eventService = (EventService) ctx.getBean("eventService");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		String getEmployeeSchedule = request
				.getParameter("getEmployeeSchedule");
		String getPatients = request.getParameter("getPatients");
		String createEvent = request.getParameter("createEvent");

		if (getEmployeeSchedule != null) {
			response.setContentType(CONTENT_TYPE);
			Integer employeeId = Integer.parseInt(request
					.getParameter("getEmployeeSchedule"));
			Employee employee = new Employee();
			employee.setId(employeeId);
			Date from = new Date(Long.parseLong(request
					.getParameter("from")));
			Date to = new Date(Long.parseLong(request
					.getParameter("to")));
			List<Event> schedule = eventService.getEvents(
					from, to, employee);

			Gson gson = new GsonBuilder().setDateFormat(DATE_FORMAT).create();
			String json = gson.toJson(schedule);

			PrintWriter out = response.getWriter();
			IOUtils.write(json, out);
			IOUtils.closeQuietly(out);

		} else if (createEvent != null) {

			Integer employeeId = Integer.parseInt(request
					.getParameter("employeeId"));
			Integer actionId = Integer.parseInt(request
					.getParameter("employeeActionSelect"));
			String date = request.getParameter("scheduleDate");
			Integer patientId = Integer.parseInt(request
					.getParameter("patientId"));
			String start = request.getParameter("startDate");
			String end = request.getParameter("endDate");

			LocalDateTime plannedStart = LocalDateTime.parse(date);
			LocalDateTime plannedEnd = LocalDateTime.parse(date);
						
			LocalTime startTime = LocalTime.parse(start);
			LocalTime endTime = LocalTime.parse(end);
			
			plannedStart = plannedStart.withHour(startTime.getHour());
			plannedStart = plannedStart.withMinute(startTime.getMinute());

			plannedEnd = plannedEnd.withHour(endTime.getHour());
			plannedEnd = plannedEnd.withMinute(endTime.getMinute());
			
			Date plannedEndStd = new Date(plannedEnd.toEpochSecond(ZoneOffset.of(CEST_OFFSET)) * 1000);
			Date plannedStartStd = new Date(plannedStart.toEpochSecond(ZoneOffset.of(CEST_OFFSET)) * 1000);
			
			Event event = new Event();
			event.setStartPlanned(plannedStartStd);
			event.setEndPlanned(plannedEndStd);
			

			logger.debug(
					"Received new schedule request  EmployeeId: [{}], ActionId: [{}], Date: [{}]",
					new Object[] { employeeId, actionId, date });

			Employee employee = new Employee();
			employee.setId(employeeId);

			Action action = new Action();
			action.setId(actionId);

			Patient patient = new Patient();
			patient.setId(patientId);
			
			event.setEmployee(employee);
			event.setAction(action);
			event.setPatient(patient);
			
			eventService.createEvent(event);

			response.sendRedirect("schedule.jsp");

		} else if (getPatients != null) {
			List<Patient> patientList = patientService.getAllPatients();
			request.setAttribute("patients", patientList);
		}

	}

}
