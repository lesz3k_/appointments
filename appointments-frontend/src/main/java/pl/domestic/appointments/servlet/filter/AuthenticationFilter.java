package pl.domestic.appointments.servlet.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Request filter that should accomplish following tasks: permit admin users to
 * access administrative browser-based panel; permit all users to access
 * services from android-based application; redirect user to login page when
 * he's not actually logged in.
 * 
 * All auth checks are performed regardless, whether session has a cookie with a
 * username parameter that is appended to response header once a given user
 * passes validity check.
 * 
 * @author Leszek_Wisniewski
 *
 */
@WebFilter("/AuthenticationFilter")
public class AuthenticationFilter implements Filter {
	private ServletContext context;

	private static String LOGIN_URL_ELEMENT = "login";
	private static String MOBILE_URL_ELEMENT = "mobile";
	private static String USERNAME_SESSION_ATTR = "username";
	private static String RESOURCE_ROOT = "lib/";
	private static String AUTH_SERVLET_NAME = "AuthenticationServlet";

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		String uri = httpRequest.getRequestURI();

		HttpSession session = httpRequest.getSession();

		// if a login request is done regardless of source (mobile or browser)
		// this performs a basic check whether any user's session is equipped
		// with a username cookie. Also, an exception for servlet url is made
		// since all requests must be allowed to it to perform actual
		// validation.
		if (uri.contains(LOGIN_URL_ELEMENT)
				|| session.getAttribute(USERNAME_SESSION_ATTR) != null
				|| uri.endsWith(AUTH_SERVLET_NAME)
				|| uri.contains(RESOURCE_ROOT)) {
			chain.doFilter(request, response);
			// disallow access to /mobile methods from browser and mobile
			// clients if unauthed
		} else if (uri.contains(MOBILE_URL_ELEMENT)
				&& session.getAttribute(USERNAME_SESSION_ATTR) == null) {
			httpResponse.setStatus(403);
		} else {
			httpResponse.sendRedirect("login.jsp");
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		context = config.getServletContext();
	}

}
