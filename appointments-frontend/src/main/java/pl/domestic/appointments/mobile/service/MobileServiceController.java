package pl.domestic.appointments.mobile.service;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.Event;
import pl.domestic.appointments.entity.HealthReport;
import pl.domestic.appointments.entity.ICD10;
import pl.domestic.appointments.entity.Patient;
import pl.domestic.appointments.service.AuthenticationService;
import pl.domestic.appointments.service.DiagnosisService;
import pl.domestic.appointments.service.EmployeeService;
import pl.domestic.appointments.service.EventService;
import pl.domestic.appointments.service.PatientService;
import pl.domestic.appointments.service.auth.exception.AuthenticationException;

@Controller
public class MobileServiceController {

	private final static String APPLICATION_JSON = "application/json; charset=UTF-8";

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private PatientService patientService;

	@Autowired
	private EventService eventService;

	@Autowired
	private AuthenticationService authService;

	@Autowired
	private DiagnosisService diagnosisService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public Employee authenticate(HttpServletRequest request,
			HttpServletResponse response) throws AuthenticationException,
			IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		boolean isUserValid = authService.validateUser(username, password,
				false);
		if (isUserValid) {
			Employee employee = employeeService.getEmployee(username);
			Cookie cookie = new Cookie("username", username);
			cookie.setMaxAge(600);
			response.addCookie(cookie);
			request.getSession().setAttribute("username", username);
			return employee;
		} else {
			response.setStatus(403);
			return null;
		}
	}

	@RequestMapping(value = "/schedule/{employeeId}/{from}/{to}", method = RequestMethod.GET, produces = APPLICATION_JSON)
	@ResponseBody
	public List<Event> getEventsForEmployee(
			@PathVariable("employeeId") Integer employeeId,
			@PathVariable("from") Long from, @PathVariable("to") Long to) {
		Employee employee = new Employee();
		employee.setId(employeeId);
		Date fromStd = new Date(from);
		Date toStd = new Date(to);
		return eventService.getEvents(fromStd, toStd, employee);
	}

	@RequestMapping(value = "/event/{eventId}/diagnosis/{diagnosisId}")
	public void addDiagnosis(@PathVariable("eventId") Integer eventId,
			@PathVariable("code") String icd10code) {
//		diagnosisService.
	}

	@RequestMapping(value = "/patient/{id}", method = RequestMethod.GET, produces = APPLICATION_JSON)
	@ResponseBody
	public Patient getPatient(@PathVariable Integer id) {
		return patientService.getPatient(id);
	}

	@RequestMapping(value = "/patient/{patientId}/report/{reportId}", method = RequestMethod.GET, produces = APPLICATION_JSON)
	@ResponseBody
	public HealthReport getPatientReport(@PathVariable Integer patientId,
			@PathVariable Integer reportId) {
		return null;
	}

	@RequestMapping(value = "/patients", method = RequestMethod.GET, produces = APPLICATION_JSON)
	@ResponseBody
	public List<Patient> getPatients() {
		return patientService.getAllPatients();
	}

	@RequestMapping(value = "/diagnosis/all", method = RequestMethod.GET, produces = APPLICATION_JSON)
	public List<ICD10> getICD10Codes() {
		return diagnosisService.getICD10CodeList();
	}
	
	@RequestMapping(value = "/event/start", method = RequestMethod.POST)
	@ResponseBody
	public Integer addEventStart(@RequestParam("eventId") Integer eventId, @RequestParam("start") Long start) {
		
		Event event = new Event();
		event.setId(eventId);
		event.setStartActual(new Date(start));
		
		return eventService.updateEventStart(event);
	}
	
	@RequestMapping(value = "/event/end", method = RequestMethod.POST)
	@ResponseBody
	public Integer addEventEnd(@RequestParam("eventId") Integer eventId, @RequestParam("end") Long end) {
		
		Event event = new Event();
		event.setId(eventId);
		event.setEndActual(new Date(end));
		
		return eventService.updateEventEnd(event);
	}

	@RequestMapping(value = "/event/{event}")
	public void updateEvent(@PathVariable("event") Event event) {
		eventService.updateEvent(event);
	}

}
