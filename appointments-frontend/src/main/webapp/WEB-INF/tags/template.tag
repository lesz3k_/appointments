<!DOCTYPE html5>
<%@ tag language="java" pageEncoding="UTF-8"%>

<%@attribute name="title"%>
<%@attribute name="header" fragment="true"%>
<%@attribute name="body" fragment="true"%>
<%@attribute name="activeTab" fragment="true" %>

<html>
<title>${title}</title>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Appointments - administration panel</title>

<!-- Bootstrap core CSS -->
<link href="lib/bootstrap-3.2.0-dist/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="lib/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="lib/bootstrap-3.2.0-dist/navbar-static-top.css"
	rel="stylesheet">


<link href="lib/bootstrap-select/bootstrap-select.min.css"
	rel="stylesheet">
	
<link href="lib/bootstrap-table/dist/bootstrap-table.min.css"
	rel="stylesheet">
	<link href="lib/jquery-ui-1.11.4/jquery-ui.min.css" rel="stylesheet">

<script src="lib/jquery-2.1.1.min.js"></script>
<script src="lib/bootstrap-3.2.0-dist/js/collapse.js"></script>
<script src="lib/bootstrap-3.2.0-dist/js/transition.js"></script>
<script src="lib/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>
<script src="lib/bootstrap-select/bootstrap-select.min.js"></script>
<script src="lib/bootstrap-select/ajaxSelectPicker.min.js"></script>
<script src="lib/moment-with-locales.js"></script>
<script src="lib/fullcalendar-2.3.1/fullcalendar.js"></script>
<script
	src="lib/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
<script src="lib/bootstrap-table/src/bootstrap-table.js" type="text/javascript"></script>
<script src="lib/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="lib/js/datetime-picker.js"></script>
<script type="text/javascript" src="lib/js/select.js"></script>
<script type="text/javascript" src="lib/fullcalendar-2.3.1/lang-all.js"></script>
	
<jsp:invoke fragment="header" />
</head>
<body>
	<!-- Static navbar -->
	<div class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">System ewidencji wizyt
					lekarskich</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
				<jsp:invoke fragment="activeTab" />					
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<jsp:invoke fragment="body" />
</body>
</html>

