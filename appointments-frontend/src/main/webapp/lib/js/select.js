$category = $('#employeeIdSelect');

var updateSelect = function() {
	$.ajax({
		type : "GET",
		url : "EmployeeServlet",
		encoding : "UTF-8",
		dataType : 'json',
		contentType : 'application/json; charset=utf-8',
		data : {
			selectedEmployeeId : $category.find(":selected").val()
		},
		success : function(data) {

			$('#employeeActionSelect').html("");
			var len = data.length;
			for (i = 0; i < len; i++) {
				var id = data[i].id;
				$('#employeeActionSelect').append(
						"<option value=" + id + ">" + data[i].action
								+ "</option>").selectpicker('refresh');
			}
		}
	});
}

$(document).ready(updateSelect);
$category.change(updateSelect);