$(function() {

	$('#datetimepicker').datetimepicker({
		language : 'pl',
		use24hours : true
	});
	$('.selectpicker').selectpicker();
	$('#table-javascript').bootstrapTable(
			{
				method : 'get',
				url : 'ScheduleServlet',
				queryParams : function queryParams(params) {
					return {
						getEmployeeSchedule : $('#employeeScheduleSelect')
								.find(":selected").val()
					};
				},
				encoding : "UTF-8",
				dataType : 'json',
				contentType : 'application/json; charset=utf-8',
				height : 520,
				pagination : true,
				striped : true,
				showColumns : true,
				showRefresh : true,
				minimunCountColumns : 2,
				columns : [ {
					field : 'patient',
					title : 'PESEL pacjenta',
					align : 'right',
					valign : 'bottom',
					sortable : true,
					formatter : function patientPeselFormatter(value) {
						return value.pesel;
					}
				}, {
					field : 'patient',
					title : 'Imię pacjenta',
					align : 'center',
					valign : 'middle',
					sortable : true,
					formatter : function patientNameFormatter(value) {
						return value.firstname + " " + value.lastname;
					}
				}, {
					field : 'action',
					title : 'Nazwa',
					align : 'left',
					valign : 'top',
					sortable : true,
					formatter : function actionNameFormatter(value) {
						return value.action;
					}
				}, {
					field : 'date',
					title : 'Data i czas',
					align : 'center',
					valign : 'middle'
				} ]
			});
});