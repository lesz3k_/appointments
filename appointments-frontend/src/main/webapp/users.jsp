<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="site"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.List"%>
<%@ page
	import="pl.domestic.appointments.service.AuthenticationService"%>
<%@ page import="pl.domestic.appointments.entity.Employee"%>
<%@ page import="org.springframework.web.context.ContextLoader"%>

<site:template>

	<jsp:attribute name="header">
</jsp:attribute>

	<jsp:attribute name="activeTab">
<li class="active"><a href="users.jsp">Zarządzanie użytkownikami</a></li>
<li><a href="schedule.jsp">Zarządzanie harmonogramami</a></li>
<li><a href="duty.jsp">Dyżury pracowników</a></li>
<li><a href="stats.jsp">Statystyki</a></li>

</jsp:attribute>

	<jsp:attribute name="body">
	
<div class="container">
	<div class="jumbotron">
		<h1>Zarządzaj użytkownikami</h1>
		<p>Wszyscy użytkownicy</p>
		
		<jsp:include page="EmployeeServlet">
			<jsp:param name="getEmployees" value="true" />
		</jsp:include>
		<c:forEach items="${employees}" var="employee">
			<table style="width: 400px">
				<tr>
				<td><c:out value="${employee.getLastname()}" /></td>
				<td>
					<form action="EmployeeServlet">
					<input type="hidden" name="delete"
										value="${employee.getId()}" />
					<button class="btn btn-sm btn-danger pull-right" type="submit">Usuń</button>
					</form>
				</td>
				</tr>
			</table>
		</c:forEach>
		<form class="form-horizontal" action="AuthenticationServlet" method="post" accept-charset="UTF-8">
		<input type="hidden" name="create_user" value="true">
		<fieldset>
				
		<!-- Form Name -->
		<legend>Dodaj nowego użytkownika</legend>
			
		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="firstname">Imię</label>  
			  <div class="col-md-4">
				  <input id="firstname" name="firstname" type="text"
									placeholder="Podaj imię" class="form-control input-md" required>
				  <span class="help-block">Tutaj możesz wprowadzić imie pracownika.</span>  
				</div>
				</div>
				
				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="lastname">Nazwisko</label>  
				  <div class="col-md-4">
				  <input id="lastname" name="lastname" type="text"
									placeholder="Nazwisko" class="form-control input-md" required>
				  <span class="help-block">Tutaj możesz wprowadzić nazwisko pracownika.</span>  
				  </div>
				</div>
				
				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="pesel">PESEL</label>  
				  <div class="col-md-4">
				  <input id="pesel" name="pesel" type="text" placeholder="PESEL"
									class="form-control input-md" required>
				  <span id="peselValidation" class="help-block">Tutaj można wprowadzić numer PESEL</span> 
				</div>
				</div>
				
				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="username">Nazwa użytkownika</label>  
				  <div class="col-md-4">
				  <input id="username" name="username" type="text"
									placeholder="Nazwa użytkownika" class="form-control input-md" required>
				  <span class="help-block">Tutaj można wprowadzić login użytkownika</span>  
				  <span class="help-block" style="color:red">${errorMessage}</span>
				  
				  </div>
				</div>
				
				<!-- Password input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="password">Hasło</label>
				  <div class="col-md-4">
				    <input id="password" name="password" type="password"
									placeholder="Hasło" class="form-control input-md" required>
				    <span class="help-block">Tutaj możesz wprowadzić hasło użytkownika</span>
				  </div>
				</div>
				
				<!-- Select Basic -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="employeeType">Rola</label>
				  <div class="col-md-4">
				  <jsp:include page="EmployeeServlet">
					<jsp:param name="getEmployeeTypes" value="true" />
				  </jsp:include>
				    <select id="employeeType" name="employeeType"
									class="form-control">
					  <c:forEach items="${employeeTypes}" var="employeeType">
				      <option value="${employeeType.getId()}">${employeeType.getType()}</option>
				      </c:forEach>
				    </select>
				  </div>
				</div>
				
				<!-- Checkbox (inline) -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="adminCheckBox">Administrator</label>
				  <div class="col-md-4">
				    <label class="checkbox-inline" for="adminCheckBox-0">
				      <input type="checkbox" name="adminCheckBox" id="adminCheckBox-0" value="true">
				      Tak
				    </label>
				  </div>
				</div>
				
				<!-- Button -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="createUser">Utwórz użytkownika</label>
				  <div class="col-md-4">
				    <button id="createUser" name="createUser"
									class="btn btn-success">Zatwierdź</button>
				  </div>
				</div>
				
				
			</fieldset>
		</form>
								
	</div>
</div>

<script type="text/javascript">
	$peselInput = $('#pesel');
	$peselInput.change(function() {
			var textLen = $peselInput.val().length;
			if(textLen < 11) {
				$('#peselValidation').html("<p style=\"color:red; font-size:14px\">PESEL musi mieć conajmniej 11 cyfr </p>");
			} else if(textLen > 11) {
				$('#peselValidation').html("<p style=\"color:red; font-size:14px\">PESEL nie może mieć więcej niż 11 cyfr </p>");
			} else {
				$('#peselValidation').html("Tutaj można wprowadzić numer PESEL");
			}
		}
	);
		
</script>
</jsp:attribute>

</site:template>
