<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="site"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<site:template>
	<jsp:attribute name="header">
		<link rel="stylesheet" href="lib/fullcalendar-2.3.1/fullcalendar.css" />
</jsp:attribute>

	<jsp:attribute name="activeTab">
<li><a href="users.jsp">Zarządzanie użytkownikami</a></li>
<li class="active"><a href="schedule.jsp">Zarządzanie harmonogramami</a></li>
<li><a href="duty.jsp">Dyżury pracowników</a></li>
<li><a href="stats.jsp">Statystyki</a></li>

</jsp:attribute>

	<jsp:attribute name="body">

	
	<div class="container">

		<!-- Main component for a primary marketing message or call to action -->
		
		<div class="jumbotron">
			<h3>Zarządzaj harmonogramem</h3>
			<p>Panel zarządzania harmonogramem pracownika.</p>
			<h4>Przeglądaj harmonogram</h4>
			<p>Wyświetl harmonogram danego użytkownika i opcjonalnie usuń
				wizytę.</p>
				<jsp:include page="EmployeeServlet">
					<jsp:param name="getEmployees" value="true" />
				</jsp:include>
				<b>Użytkownik:</b> <br> 
				<select id="employeeScheduleSelect" name="getEmployeeSchedule"
					class="selectpicker">

				<jsp:useBean id="dateValue" class="java.util.Date" />
				<c:forEach items="${employees}" var="emp">
					<option value="${emp.getId()}">${emp.getFirstname()} ${emp.getLastname()}</option>
				</c:forEach>	
				</select> 
				<br>
				<br>
			<div class="well well-sm">
				<div id="calendar"></div>
			</div>
			
			<div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog modal-lg">
			        <div class="modal-content" style="padding-left: 20px">
			        	<h3 id="eventTitle"></h3>
							<form action="ScheduleServlet" method="post" accept-charset="UTF-8">
								<input type="hidden" name="createEvent" value="true">
								<input type="hidden" id="employeeId" name="employeeId" value="1">
								<input type="hidden" id="scheduleDate" name="scheduleDate">
									<fieldset> 
										<b>Akcja:</b> <br> 
										<select id="employeeActionSelect" name="employeeActionSelect"
														class="selectpicker">
										</select>
										<br>
					
										<b>Pacjent:</b>
										
										<jsp:include page="ScheduleServlet">
											<jsp:param name="getPatients" value="true" />
										</jsp:include>
										<br>
										<select id="patientSelect" name="patientId" class="selectpicker">
											<c:forEach items="${patients}" var="patient">
												<option value="${patient.getId()}">${patient.getFirstname()} ${patient.getLastname()}</option>
											</c:forEach>
										</select>
										<br>
										<div class="row">
											<div class="form-group">
											  <label class="col-md-4 control-label" for="autoSchedule">Automatyczne dopasowanie długości wizyty</label>
											  <div class="col-md-4">
											    <label class="checkbox-inline" for="autoSchedule">
											      <input type="checkbox" name="adminCheckBox" id="autoSchedule" value="true">
											      Tak
											    </label>
											  </div>
											</div>
										</div>
										<div class="row">
											<div class='col-sm-3'>
												<div class="form-group">
													<b>Start:</b>
													<div class='input-group date' id='starttimepicker'>
														<input type='text' class="form-control" name="startDate" /> <span
																			class="input-group-addon"><span
																			class="glyphicon glyphicon-calendar"></span> </span>
													</div>
												</div>
											</div>
											<div class='col-sm-3'>
												<div class="form-group">
													<b>Koniec:</b>
													<div class='input-group date' id='endtimepicker'>
														<input type='text' class="form-control" name="endDate" /> <span
																			class="input-group-addon"><span
																			class="glyphicon glyphicon-calendar"></span> </span>
													</div>
												</div>
											</div>
										</div>
										<button class="btn btn-lg btn-primary">Zatwierdź
											&raquo;</button>
									</fieldset>
						</form>
			    </div>
			</div>
		</div>
			
			
			
	</div>
		
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			
			var schedule = null;

			function renderCalendar() {
				$('#calendar').fullCalendar({
					header : {
						left : 'prev,next today',
						center : 'title',
						right : 'agendaWeek,agendaDay'
					},
					defaultView: 'agendaWeek',
					buttonIcons : false,
					lang : 'pl',
					minTime : '07:00:00',
					maxTime : '22:00:00',
					slotDuration : '00:05:00',
					selectable : true,
					events: function(start, end, timezone, callback) {
				        $.ajax({
				            url: 'http://localhost:8080/appointments-frontend/ScheduleServlet',
				            dataType: 'json',
				            minTime: '07:00:00',
				            maxTime: '19:00:00',
				            data: {
				            	getEmployeeSchedule : $('#employeeScheduleSelect').val(),
				            	from : start.unix() + '000',
				            	to : end.unix() + '000'
				            },
				            success: function(doc) {
				                var events = [];
				                $.each(doc, function(f, val) {
				                		events.push({
						                    title: val.patient.firstname + ' ' + val.patient.lastname + '\n' + val.action.action,
						                    start: val.startPlanned,
						                    end: val.endPlanned
					                	});
				                });
				                callback(events);
				            }
				        });
				    },
				    dayClick: function(date, jsEvent, view) {

						var username = $("#employeeScheduleSelect").find(":selected").text();
						
						var hrs = date.hour();
						var mins = date.minute();
				    	
				    	$("#eventModal").modal();
						$("#eventTitle").html("<h3>Dodaj wizytę dla użytkownika " + username + " na dzień " + date.format() + "</h3>");
						$("#scheduleDate").val(date.format());
						$("#starttimepicker").data("DateTimePicker").setDate(new Date(date.year(), date.month(), date.date(), hrs, mins));
				    	
						$("#autoSchedule").change(function () {
						    if ($('#autoSchedule').is(':checked')) {
						        var startDate = $('#starttimepicker').data("DateTimePicker").getDate();
						        
						        $.ajax({
									type : "GET",
									url : "TimeslotServlet",
									encoding : "UTF-8",
									dataType : 'json',
									contentType : 'application/json; charset=utf-8',
									data : {
										recommendTimeslot : "true",
										from : moment().subtract(30, 'day').unix(),
										to : moment().unix()
									},
									success : function(data) {
										var endDate = startDate.add(data.timeslot, 'seconds');
										$('#endtimepicker').data("DateTimePicker").setDate(endDate);
									}
								});
						        
						    } 
						});

				    }
				});
			}
			renderCalendar();

			$('#employeeScheduleSelect').on('change', function() { $('#calendar').fullCalendar('refetchEvents')});
			$('#employeeScheduleSelect').on('change', function() { $('#employeeId').val($('#employeeScheduleSelect').find(":selected").val())});
		});
		
		$category = $('#employeeScheduleSelect');
		var updateSelect = function() {
				$.ajax({
					type : "GET",
					url : "EmployeeServlet",
					encoding : "UTF-8",
					dataType : 'json',
					contentType : 'application/json; charset=utf-8',
					data : {
						selectedEmployeeId : $category.find(":selected").val()
					},
					success : function(data) {
						$('#employeeActionSelect').html("");
						var len = data.length;
						for (i = 0; i < len; i++) {
							var id = data[i].id;
							$('#employeeActionSelect').append(
									"<option value="+id+">" + data[i].action + "</option>")
									.selectpicker('refresh');
						}
					}
				});
		}
		
		$(document).ready(updateSelect);
		$category.change(updateSelect);
		$('#starttimepicker').datetimepicker({
			language : 'pl',
			pickDate : false,
			use24hours : true
		});
		$('#endtimepicker').datetimepicker({
			language : 'pl',
			pickDate : false,
			use24hours : true
		});
		
		function convertDateToISO8601(date) {
			return (date.substring(6,10) + '-' + 
					date.substring(3, 5) + '-' + 
					date.substring(0, 2) + 'T' + 
					date.substring(11, 17) + ':00').toString();
		}
		
		
	</script>
	
	
</jsp:attribute>
</site:template>