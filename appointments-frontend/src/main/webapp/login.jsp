<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link href="lib/bootstrap-3.2.0-dist/css/bootstrap.min.css"
	rel="stylesheet">
<link href="lib/bootstrap-3.2.0-dist/signin.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Appointments - login page</title>
</head>
<body>
	<div class="container">

		<form class="form-signin" role="form" action="AuthenticationServlet"
			method="post">
			<h2 class="form-signin-heading">Panel administracyjny</h2>
			<input type="username" class="form-control"
				placeholder="Nazwa użytkownika" name="username" required autofocus>
			<input type="password" class="form-control" name="password"
				placeholder="Hasło" required>
			<div class="checkbox">
				<label> <input type="checkbox" value="remember-me">
					Pamiętaj dane logowania
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Zaloguj!</button>
			<p style="color:red">
			<%
				if (null != request.getAttribute("errorMessage")) {
					out.println(request.getAttribute("errorMessage"));
				}
			%>
			</p>
		</form>

	</div>

</body>
</html>