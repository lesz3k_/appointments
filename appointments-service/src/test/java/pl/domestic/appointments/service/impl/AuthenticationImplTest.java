package pl.domestic.appointments.service.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.UserCredentials;
import pl.domestic.appointments.service.AuthenticationService;
import pl.domestic.appointments.service.auth.exception.AuthenticationException;
import pl.domestic.appointments.service.auth.exception.UserAlreadyExistsException;

@ContextConfiguration("classpath:test-service.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthenticationImplTest {
	
	@Autowired
	private AuthenticationService facade;
	
	@Test
	public void testValidateUser() throws AuthenticationException {
		boolean validation = facade.validateUser("a", "b", false);
		
		assertTrue(validation);
	}
	
	@Test
	@Ignore
	public void testCreateNewUser() throws AuthenticationException, UserAlreadyExistsException {
		
		Employee employee = new Employee();
		
		facade.createNewUser(employee);
		
		boolean valid = facade.validateUser("adaada", "test", false);
		
		assertTrue(valid);
	}
	
	@Test
	public void testGetEmployees() throws AuthenticationException {
		 
//		List<Employee> employees = facade.listAllUsers();
//		
//		System.out.println(employees);
//		
//		assertNotNull(employees);
	}
	
	@Ignore
	@Test
	public void testDeleteEmployees() throws AuthenticationException {
		
	//	facade.createNewUser(1, "wiejar1", "test", "Jarosław", "Wietek", "88888111812");
		
//		facade.validateUser("wiejar1", "test");
//		
//		facade.deleteEmployee(8);
		
	}

}
