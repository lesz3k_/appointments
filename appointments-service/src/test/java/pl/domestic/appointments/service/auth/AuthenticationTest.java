package pl.domestic.appointments.service.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.apache.commons.io.IOUtils;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;

import com.mchange.util.AssertException;

public class AuthenticationTest {

	private AuthenticationUtil authorization;

	@Before
	public void init() {
		authorization = new AuthenticationUtil();
	}

	@Test
	public void testHashConflict() {

	}

	@Test
	public void testCreateHash() throws NoSuchAlgorithmException,
			InvalidKeySpecException {
		
		String password = "admin";
		
		String hash = AuthenticationUtil.createHash(password);
		
		System.out.println(hash);
		
		assertNotNull(hash);
	}
	
	@Test
	public void testHashCorrectness() throws NoSuchAlgorithmException, InvalidKeySpecException {
		String password = "test";
		
		String hash = AuthenticationUtil.createHash(password);
		
		assertNotNull(hash);
		
		boolean isValid = AuthenticationUtil.validatePassword(password, hash);
		
		assertTrue(isValid);
	}

}
