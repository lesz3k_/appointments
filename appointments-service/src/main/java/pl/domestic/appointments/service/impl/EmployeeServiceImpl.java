package pl.domestic.appointments.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import pl.domestic.appointments.dao.EmployeeDAO;
import pl.domestic.appointments.dao.EmployeeTypeDAO;
import pl.domestic.appointments.dao.UserCredentialsDAO;
import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.EmployeeType;
import pl.domestic.appointments.service.EmployeeService;

@Transactional
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private EmployeeTypeDAO employeeTypeDAO;
	
	@Autowired
	private UserCredentialsDAO userCredentialsDAO;

	@Override
	public Employee getEmployee(String username) {
		return employeeDAO.getEmployeeByUsername(username);
	}

	@Override
	public List<Employee> getAllEmployees() {
		return employeeDAO.getEmployees();
	}

	@Override
	public List<EmployeeType> getEmployeeTypes() {
		return employeeTypeDAO.getEmployeeTypes();
	}

	@Override
	public void deleteEmployee(Integer id) {
		userCredentialsDAO.deleteCredentialsById(id);
		employeeDAO.deleteEmployeeById(id);
	}

}
