package pl.domestic.appointments.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import pl.domestic.appointments.dao.EmployeeDAO;
import pl.domestic.appointments.dao.UserCredentialsDAO;
import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.UserCredentials;
import pl.domestic.appointments.service.AuthenticationService;
import pl.domestic.appointments.service.auth.AuthenticationUtil;
import pl.domestic.appointments.service.auth.exception.AuthenticationException;
import pl.domestic.appointments.service.auth.exception.UserAlreadyExistsException;

@Transactional
public class AuthenticationServiceImpl implements
		AuthenticationService {

	private static Logger logger = LoggerFactory
			.getLogger(AuthenticationServiceImpl.class);

	@Autowired
	private UserCredentialsDAO userCredentialsDAO;

	@Autowired
	private EmployeeDAO employeeDAO;

	@Autowired
	private PlatformTransactionManager transactionManager;

	@Override
	public void createNewUser(Employee employee)
			throws AuthenticationException, UserAlreadyExistsException {
		// logger.debug("Creating user [{}]", employee.getUsername());

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		boolean exists = userCredentialsDAO.exists(employee
				.getUserCredentials().getUsername());
		if (exists) {
			throw new UserAlreadyExistsException(
					"User already exists in the database!");
		}

		employeeDAO.storeEmployee(employee);
		Integer id = employee.getId();

		UserCredentials userCredentials = employee.getUserCredentials();
		userCredentials.setId(id);

		userCredentialsDAO.storeUser(userCredentials);

		transactionManager.commit(status);

	}

	@Override
	public boolean validateUser(String username, String password,
			boolean adminUserExpected) throws AuthenticationException {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		String hash = userCredentialsDAO.retrieveHash(username);
		Boolean isAdmin = userCredentialsDAO.getAdminFlag(username);

		boolean isValid = false;

		if (hash != null) {
			try {
				isValid = AuthenticationUtil.validatePassword(password, hash);
				transactionManager.commit(status);
				if (!adminUserExpected) {
					return isValid;
				} else {
					return isValid && isAdmin;
				}

			} catch (NoSuchAlgorithmException e) {
				logger.error("Could not validate user [{}]", username);
				transactionManager.rollback(status);
				throw new AuthenticationException(e);
			} catch (InvalidKeySpecException e) {
				logger.error("Could not validate user [{}]", username);
				transactionManager.rollback(status);
				throw new AuthenticationException(e);
			}
		}
		return false;
	}

	@Override
	public UserCredentials generateCredentials(String username, String password)
			throws AuthenticationException {
		UserCredentials userCredentials = new UserCredentials();
		userCredentials.setUsername(username);

		String hash;
		try {
			hash = AuthenticationUtil.createHash(password);
			userCredentials.setHash(hash);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.error("Could not generate hash.");
			throw new AuthenticationException(e);
		}

		return userCredentials;
	}

	public void setUserCredentialsDAO(UserCredentialsDAO userCredentialsDAO) {
		this.userCredentialsDAO = userCredentialsDAO;
	}
}
