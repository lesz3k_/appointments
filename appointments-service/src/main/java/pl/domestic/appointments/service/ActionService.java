package pl.domestic.appointments.service;

import java.util.List;

import pl.domestic.appointments.entity.Action;

public interface ActionService {
	
	public List<Action> getActions(Integer id);

}
