package pl.domestic.appointments.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.domestic.appointments.dao.DiagnosisDAO;
import pl.domestic.appointments.entity.ICD10;
import pl.domestic.appointments.service.DiagnosisService;

public class DiagnosisServiceImpl implements DiagnosisService {
	
	@Autowired
	private DiagnosisDAO dao;

	@Override
	public List<ICD10> getICD10CodeList() {
		return dao.getICD10CodeList();
	}

}
