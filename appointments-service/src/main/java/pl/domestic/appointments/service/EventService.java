package pl.domestic.appointments.service;

import java.sql.Date;
import java.util.List;

import org.joda.time.DateTime;

import pl.domestic.appointments.entity.DiagnosisFeedback;
import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.Event;

/**
 * Base service for operations on events stored in employee schedules.
 * 
 * @author Leszek_Wisniewski
 *
 */
public interface EventService {
	
	public List<Event> getEvents(Date from, Date to, Employee employee);

	public void createEventList(List<Event> eventList);

	public void createEvent(Event event);

	public Integer updateEventStart(Event event);

	public void updateEventNoshow(Event event);

	public DiagnosisFeedback getTopDiagnosisAndTimeslot(Date from, Date to);

	public void updateEvent(Event event);

	public Integer updateEventEnd(Event event);

}
