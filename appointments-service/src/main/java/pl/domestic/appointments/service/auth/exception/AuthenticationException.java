package pl.domestic.appointments.service.auth.exception;

public class AuthenticationException extends Exception {

	public AuthenticationException(Throwable cause) {
		super(cause);
	}
	
	public AuthenticationException(String message) {
		super(message);
	}

	public AuthenticationException(Throwable cause, String message) {
		super(message, cause);
	}

}
