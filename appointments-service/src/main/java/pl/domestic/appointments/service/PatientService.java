package pl.domestic.appointments.service;

import java.util.List;

import pl.domestic.appointments.entity.Patient;

public interface PatientService {
	
	public void createPatient(Patient patient);

	public Patient getPatient(Integer id);

	public List<Patient> getAllPatients();

}
