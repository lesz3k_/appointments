package pl.domestic.appointments.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.domestic.appointments.dao.ActionDAO;
import pl.domestic.appointments.entity.Action;
import pl.domestic.appointments.service.ActionService;

public class ActionServiceImpl implements ActionService {
	
	@Autowired
	private ActionDAO dao;
	
	@Override
	public List<Action> getActions(Integer id) {
		return dao.getActions(id);
	}
}
