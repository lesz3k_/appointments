package pl.domestic.appointments.service;


import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.UserCredentials;
import pl.domestic.appointments.service.auth.exception.AuthenticationException;
import pl.domestic.appointments.service.auth.exception.UserAlreadyExistsException;

public interface AuthenticationService {
	
	/**
	 * Creates new user in system.
	 * @param username as user handle.
	 * @param password for the user to authorize.
	 * @throws AuthenticationException //JW Shouldn't there be another exception (e.g. InvalidNewUserException, DarkRingException) for this case?
	 * @throws UserAlreadyExistsException 
	 */
	public void createNewUser(Employee empl) throws AuthenticationException, UserAlreadyExistsException;
	
	/**
	 * Validates user credentials in the system.
	 * @param username to be validated.
	 * @param password to be validated.
	 * @return true if credentials valid; false if not.
	 */
	public boolean validateUser(String username, String password, boolean admin) throws AuthenticationException;

	/**
	 * Generates user credentials from username/password pair.
	 * @param username
	 * @param password
	 * @return
	 * @throws AuthenticationException 
	 */
	public UserCredentials generateCredentials(String username, String password) throws AuthenticationException;

}
