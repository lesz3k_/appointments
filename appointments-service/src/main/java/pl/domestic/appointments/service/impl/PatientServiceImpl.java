package pl.domestic.appointments.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import pl.domestic.appointments.dao.PatientDAO;
import pl.domestic.appointments.entity.Patient;
import pl.domestic.appointments.service.PatientService;

@Transactional
public class PatientServiceImpl implements PatientService {
	
	@Autowired
	private PatientDAO patientDAO;

	@Override
	public void createPatient(Patient patient) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Patient getPatient(Integer id) {
		return patientDAO.getPatient(id);
	}

	@Override
	public List<Patient> getAllPatients() {
		return patientDAO.getAllPatients();
	}

	
}
