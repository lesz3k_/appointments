package pl.domestic.appointments.service.impl;

import java.sql.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import pl.domestic.appointments.dao.EventsDAO;
import pl.domestic.appointments.entity.DiagnosisFeedback;
import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.Event;
import pl.domestic.appointments.service.EventService;

public class EventServiceImpl implements EventService {

	@Autowired
	private EventsDAO eventsDAO;

	@Override
	public void createEventList(List<Event> eventList) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createEvent(Event event) {
		eventsDAO.createEvent(event);
	}

	@Override
	public Integer updateEventStart(Event event) {
		return eventsDAO.updateEventStart(event);
	}

	@Override
	public Integer updateEventEnd(Event event) {

		return eventsDAO.updateEventEnd(event);

	}

	@Override
	public void updateEventNoshow(Event event) {
		// TODO Auto-generated method stub

	}

	@Override
	public DiagnosisFeedback getTopDiagnosisAndTimeslot(Date from, Date to) {
		return eventsDAO.getTopDiagnosisAndTimeslot(from, to);
	}

	@Override
	public List<Event> getEvents(Date from, Date to, Employee employee) {
		return eventsDAO.getEvents(from, to, employee);
	}

	@Override
	public void updateEvent(Event event) {
		eventsDAO.updateEvent(event);
	}

}
