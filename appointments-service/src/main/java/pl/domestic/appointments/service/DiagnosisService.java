package pl.domestic.appointments.service;

import java.util.List;

import pl.domestic.appointments.entity.ICD10;

/**
 * Base interface for retrieving ICD10-standard codes and their description.
 * @author Leszek_Wisniewski
 *
 */
public interface DiagnosisService {
	
	/**
	 * Method for retrieving all of the codes inside ICD-10 standard with their descriptions.
	 * @return All of the code-description key-value pairs.
	 */
	public List<ICD10> getICD10CodeList();

}
