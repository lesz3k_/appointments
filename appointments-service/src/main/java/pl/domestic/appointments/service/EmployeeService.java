package pl.domestic.appointments.service;
import java.util.List;

import pl.domestic.appointments.entity.Employee;
import pl.domestic.appointments.entity.EmployeeType;

public interface EmployeeService {
	
	public Employee getEmployee(String username);

	public List<Employee> getAllEmployees();

	public List<EmployeeType> getEmployeeTypes();

	public void deleteEmployee(Integer id);

}
